/**
 * Configure environment variables and constants
 */

let config = require('config').get('config');

const DEFAULT_ENV = 'production';


let { ENV_TYPE = DEFAULT_ENV } = process.env;

if (!ENV_TYPE in config)
    ENV_TYPE = DEFAULT_ENV;

let serverConfig = Object.assign({ ENV_TYPE }, config[ENV_TYPE]);

module.exports = serverConfig;
