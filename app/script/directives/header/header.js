angular.directive('header', function(){
    return {
        templateUrl: '/scripts/directives/header/header.html',
        restrict: 'E',
        replace: true,
        controller: function () {

        },
        link: function () {
            $(document).on('click', '.navbar-collapse.in', function (e) {
                if ($(e.target).is('a') || $(e.target).is('button')) {
                    $(this).collapse('hide');
                }
            });
        }
    }
})