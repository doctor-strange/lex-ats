'use strict';

var app = angular.module('lexAtsApp', [
  'ngAnimate',
  'ngCookies',
  'ngMessages',
  'ngResource',
  'ngRoute',
  'ngSanitize',
  'ui.router',
  'ui.bootstrap',
  'ngProgress',
  'duScroll',
  'angularLazyImg',
  'slick',
  'ngMaterial',
  'ngStorage',
  'thatisuday.dropzone',
  'rzModule',
  'multipleSelect',
  'restangular',
  'angular-loading-bar',
  'angularMoment',
  'mgcrea.ngStrap'
]).
config(['$locationProvider', '$routeProvider','$urlRouterProvider', '$stateProvider', function($locationProvider, $routeProvider, $urlRouterProvider, $stateProvider) {
  $locationProvider.hashPrefix('!');


  $stateProvider

    .state('main', {
      url: '',
      abstract: true,
      templateUrl: "../../views/container.html"
    })
    .state('login', {
      url: '/login',
      templateUrl: "../../views/login/login.html",
      controller: "loginCtrl",
      controllerAs: "lgc"
    })
    .state('atsSelect', {
      url: '/select_ats',
      templateUrl: "../../views/ats/ats_select.html",
      controller: "atsSelectCtrl",
      controllerAs: "atsc"
    })

    $urlRouterProvider.otherwise('/login');
}]);